package br.com.reignited.yumfood.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YumfoodAuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(YumfoodAuthApplication.class, args);
	}

}
